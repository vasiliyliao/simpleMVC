<?php
    class Route
    {
        static private $pattern_par=[];
        static private $status;
        static private $func;

        static public $request_params=[];
        static public $route_name=[];

        static public function pattern($data)
        {
            foreach($data as $key => $value){
                self::$pattern_par[$key]=$value;
            }
        }

        static public function index($action, $where=null)
        {
           self::$route_name['/']='';
           if(!isset($_SERVER['REDIRECT_URL'])){
                self::loadAction($action);
           }else{
                return false;
           }
        }
        static public function any($uri, $action, $where=null)
        {
            self::addRoute($_SERVER['REQUEST_METHOD'],$uri, $action, $where);
        }
        static public function get($uri, $action, $where=null)
        {
            self::addRoute('GET',$uri, $action, $where);
        }

        static public function post($uri, $action, $where=null)
        {
            self::addRoute('POST',$uri, $action, $where);
        }

        static public function error404($funt){
            if(self::$status){
                self::runAction();
                return false;
            }else{
                header("HTTP/1.0 404 Not Found");
                echo $funt();
            }
        }
        static private function addRoute($method,$CurrentUri, $action, $where){
            if(is_array($action)){
                if(isset($action['as'])){
                    self::$route_name[$action['as']]=$CurrentUri;
                    $action=$action[0];
                }
            }
            if(self::$status){
                return false;
            }
            if($_SERVER['REQUEST_METHOD']== $method AND isset($_SERVER['REDIRECT_URL']) ){
                $CurrentUriTmp=explode('/', $CurrentUri);
                $NowUri=explode('/', $_SERVER['REDIRECT_URL']);
                if(count($CurrentUriTmp) != count($NowUri)){
                    return false;
                }
                foreach($CurrentUriTmp as $key => $value){
                    if($value != $NowUri[$key]){
                        $tmpPar=null;

                        if( isset($where[$value]) ){
                            $tmpPar=$where[$value];
                        }elseif(isset(self::$pattern_par[$value])){
                            $tmpPar=self::$pattern_par[$value];
                        }else{
                            return false;
                        }

                        if( !preg_match($tmpPar,$NowUri[$key]) )
                        {
                            self::$request_params=[];
                            return false;
                        }
                        self::$request_params[$value]=$NowUri[$key];
                    }
                }
                self::loadAction($action);
            }
        }
        static private function loadAction($action)
        {
            self::$status=true;
            self::$func=$action;
        }
        static private function runAction()
        {
            $action=self::$func;
            if(is_string($action)){
                $tmp=explode('@', $action);
                $controller = new $tmp[0]();
                echo $controller->{$tmp[1]}();
            }else{
                echo $action(self::$request_params);
            }
        }
    }

?>