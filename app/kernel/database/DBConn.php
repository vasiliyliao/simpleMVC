<?php
class DBConn
{
    static protected $db='';
    public function __construct()
    {
        try
        {
            self::$db = new PDO(
                SQL_DEVICE.':host='.SQL_SERVER_IP.';dbname='.SQL_SERVER_DB.';charset=utf8',
                SQL_SERVER_USER,
                SQL_SERVER_PWD);
            self::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch(PDOException $e)
        {
            die($e->getMessage());
        }
    }
    public static function getDB()
    {
    	if(!self::$db){
    		new dbConn();
    	}
        //return self::$db;
    }
}
?>