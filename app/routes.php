<?php
Route::pattern([':id'=>'/^[0-9]*$/']);
Route::index(function(){
	$tmpGr=['黃色阿公','藍色阿公','綠色阿公','紫色阿公','紅色阿公'];
	$tmpGrBody=['阿公天天起來做運動','阿公天天起來尿尿','阿公天天起來找阿罵','阿公天天起來拜天公','阿公天天起來煮飯'];
	$tmpGrshBody=['做運動','尿尿','阿罵','拜天公','煮飯'];
	$tmp=['lib/cleanblog/img/home-bg.jpg','lib/cleanblog/img/post-bg.jpg','lib/cleanblog/img/contact-bg.jpg','lib/cleanblog/img/about-bg.jpg'];
	$data=[
		'title'=>'Demo|Index',
		'img' => $tmp[rand(0,3)],
		'GrBody'=>$tmpGrBody,
		'GrshBody'=>$tmpGrshBody,
		'grandPar' =>$tmpGr,
		'blogTitle' => 'Learn PHP MVC',
		'blogIntr'	=> '輕鬆學會MVC'
	];
 	return view('home.index',$data);
});
Route::get('/about.asp',['as'=> 'about.index',function(){
	$data=[
		'title'=>'Demo|About',
		'blogTitle' => '關於',
		'blogIntr'	=> '輕鬆學,輕鬆用'
	];
	return view('about.index',$data);
}]);
Route::get('/contact.asp',['as'=>'contact.create',function(){
	$data=[
		'title'=>'Demo|Contact',
		'blogTitle' => '連絡我們',
		'blogIntr'	=> '我們卻不會聯絡你'
	];
	return view('contact.create',$data);
}]);
Route::get('/random.asp',['as'=> 'posts.random',function(){
	$data=[
		'title'=>'Demo|Random',
		'blogTitle' => rand(1,1000000),
		'blogIntr'	=> rand(1,1000000)
	];
	return view('posts.show',$data);
}]);
Route::get('/posts.asp/:id',['as'=> 'posts.show',function($params){
	$data=[
		'title'=>'Demo|id為'.$params[':id'].'的個人文章',
		'blogTitle' => $params[':id'],
		'blogIntr'	=> rand(1,1000000)
	];
	return view('posts.show',$data);
}]);

Route::get('/mytest.asp',function(){
	//$mo=new Model;
	//$tmp=$mo->all()->take(4)->get();

	//var_dump( $tmp);
	//return $mo['id'];
	return 'test';
});
Route::get('/my.asp','helloController@abc');
Route::error404(function(){
	return view('error.404');
});

