<?= view('layout.compoent.html-start',['title'=>$title]); ?>
     <?= view('layout.compoent.nav');?>
     <?= view('layout.compoent.body-head',['img'=>$img,'blogTitle'=>$blogTitle,'blogIntr'=>$blogIntr ]); ?>

     <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <?php foreach(range(1,5) as $value):?>
                <div class="post-preview">
                    <a href="<?= route('posts.show',[':id'=>$value]) ?>">
                        <h2 class="post-title">
                           <?= $GrBody[rand(0,4)] ?>
                        </h2>
                        <h3 class="post-subtitle">
                            <?= $GrshBody[rand(0,4)] ?>
                        </h3>
                    </a>
                    <p class="post-meta">更新於 <a href="#"><?= $grandPar[rand(0,4)] ?></a> @ 2015</p>
                </div>
                <?php endforeach;?>

                <hr>
                <!-- Pager -->
                <ul class="pager">
                    <li class="next">
                        <a href="#">Older Posts &rarr;</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

     <?= view('layout.compoent.footer'); ?>
<?= view('layout.compoent.html-end'); ?>